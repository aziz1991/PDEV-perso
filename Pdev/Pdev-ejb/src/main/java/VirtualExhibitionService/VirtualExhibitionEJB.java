package VirtualExhibitionService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Pdev.VirtualExhibition;

/**
 * Session Bean implementation class VirtualExhibitionEJB
 */
@Stateless
@LocalBean
public class VirtualExhibitionEJB implements VirtualExhibitionEJBRemote {
	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public VirtualExhibitionEJB() {
        // TODO Auto-generated constructor stub
    }
    @Override
	public void addExhibition(VirtualExhibition exhibition) {
		// TODO Auto-generated method stub
		em.persist(exhibition);
	}

	@Override
	public VirtualExhibition findExhibitionByID(int idGv) {
		
		return em.find(VirtualExhibition.class, idGv);
	}

	@Override
	public void deleteExhibition(VirtualExhibition exhibition) {
		em.remove(em.merge(exhibition));
	}

	@Override
	public void updateExhibition(VirtualExhibition exhibition) {
		em.merge(exhibition);
		
	}
	@Override
	public VirtualExhibition findExhibitionByType(String Type){
		return em.createQuery("select e from VirtualExhibition e where e.Type=:x",VirtualExhibition.class)
				.setParameter("x",Type).getSingleResult();
	}

}

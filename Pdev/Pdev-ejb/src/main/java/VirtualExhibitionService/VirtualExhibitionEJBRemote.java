package VirtualExhibitionService;

import javax.ejb.Remote;

import Pdev.VirtualExhibition;

@Remote
public interface VirtualExhibitionEJBRemote {
	public void addExhibition(VirtualExhibition exhibition);
	public VirtualExhibition findExhibitionByID(int idGv);
	public void deleteExhibition(VirtualExhibition exhibition);
	public void updateExhibition(VirtualExhibition exhibition);
	public VirtualExhibition findExhibitionByType(String Type);
}

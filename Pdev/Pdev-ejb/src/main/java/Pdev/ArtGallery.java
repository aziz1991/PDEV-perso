package Pdev;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ArtGallery
 *
 */
@Entity

public class ArtGallery implements Serializable {

	   
	@Id
	private int idArtG;
	private int Price;
	private Long Description;
	private String State;
	private String Image;
	private static final long serialVersionUID = 1L;
	@OneToOne
	private Adress adress;
	@OneToMany(mappedBy="artgallery")
	private List<Location> listLocation; 
	public ArtGallery() {
		super();
	}   
	public int getIdArtG() {
		return this.idArtG;
	}

	public void setIdArtG(int idArtG) {
		this.idArtG = idArtG;
	}   
	 
	public int getPrice() {
		return this.Price;
	}

	public void setPrice(int Price) {
		this.Price = Price;
	}   
	public Long getDescription() {
		return this.Description;
	}

	public void setDescription(Long Description) {
		this.Description = Description;
	}   
	public String getState() {
		return this.State;
	}

	public void setState(String State) {
		this.State = State;
	}   
	public String getImage() {
		return this.Image;
	}

	public void setImage(String Image) {
		this.Image = Image;
	}
	public Adress getAdress() {
		return adress;
	}
	public void setAdress(Adress adress) {
		this.adress = adress;
	}
   
}

package Pdev;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: EtudiantCousID
 *
 */
@Embeddable
public class LocationID implements Serializable {

	
	private int idArtGPK;
	private int idUserPK;
	private static final long serialVersionUID = 1L;
	
	public LocationID() {
		super();
	}   
	
	public int getIdArtGPK() {
		return idArtGPK;
	}

	public void setIdArtGPK(int idArtGPK) {
		this.idArtGPK = idArtGPK;
	}

	public int getIdUserPK() {
		return idUserPK;
	}

	public void setIdUserPK(int idUserPK) {
		this.idUserPK = idUserPK;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idArtGPK;
		result = prime * result + idUserPK;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationID other = (LocationID) obj;
		if (idArtGPK != other.idArtGPK)
			return false;
		if (idUserPK != other.idUserPK)
			return false;
		return true;
	}

	
	
   
}

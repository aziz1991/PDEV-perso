package Pdev;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import javax.persistence.*;

import Pdev.Artist;
import Pdev.Client;

/**
 * Entity implementation class for Entity: ArtWork
 *
 */
@Entity

public class ArtWork implements Serializable {

	   
	@Id
	private int idArt;
	private String Type;
	private int Price;
	private Long Description;
	private String Image;
	private String Title;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Artist artist;
	@ManyToOne
	private Client client;
	public ArtWork() {
		super();
	}   
	public int getIdArt() {
		return this.idArt;
	}

	public void setIdArt(int idArt) {
		this.idArt = idArt;
	}   
	public String getType() {
		return this.Type;
	}

	public void setType(String Type) {
		this.Type = Type;
	}   
	public int getPrice() {
		return this.Price;
	}

	public void setPrice(int Price) {
		this.Price = Price;
	}   
	public Long getDescription() {
		return this.Description;
	}

	public void setDescription(Long Description) {
		this.Description = Description;
	}   
	public String getImage() {
		return this.Image;
	}

	public void setImage(String Image) {
		this.Image = Image;
	}   
	public String getTitle() {
		return this.Title;
	}

	public void setTitle(String Title) {
		this.Title = Title;
	}
	public Artist getArtist() {
		return artist;
	}
	public void setArtist(Artist artist) {
		this.artist = artist;
	}
   
}

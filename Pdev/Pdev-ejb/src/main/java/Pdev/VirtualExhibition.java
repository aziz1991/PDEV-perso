package Pdev;

import java.io.Serializable;
import java.lang.Long;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: VirtualExhibition
 *
 */
@Entity

public class VirtualExhibition implements Serializable {

	   
	@Id
	private int idGv;
	private String Description;
	private int Price;
	private String Type;
	private static final long serialVersionUID = 1L;

	public VirtualExhibition() {
		super();
	}   
	public int getIdGv() {
		return this.idGv;
	}

	public void setIdGv(int idGv) {
		this.idGv = idGv;
	}   
	public String getDescription() {
		return this.Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}   
	public int getPrice() {
		return this.Price;
	}

	public void setPrice(int Price) {
		this.Price = Price;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
   
}

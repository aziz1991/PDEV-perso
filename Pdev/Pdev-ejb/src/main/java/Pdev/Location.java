package Pdev;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Location
 *
 */
@Entity

public class Location implements Serializable {

	   
	@EmbeddedId
	private LocationID locationid ;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn (name="idArtGPK",insertable=false,updatable=false)
	private ArtGallery artgallery ;
	@ManyToOne
	@JoinColumn ( name="idUserPK", insertable=false,updatable=false)
	private Artist artist2;
	
	

	public Location() {
		super();
	}   

	public LocationID getLocationid() {
		return locationid;
	}
	public void setLocationid(LocationID locationid) {
		this.locationid = locationid;
	}
	public ArtGallery getArtgallery() {
		return artgallery;
	}
	public void setArtgallery(ArtGallery artgallery) {
		this.artgallery = artgallery;
	}
	public Artist getArtist2() {
		return artist2;
	}
	public void setArtist2(Artist artist2) {
		this.artist2 = artist2;
	}
   
}

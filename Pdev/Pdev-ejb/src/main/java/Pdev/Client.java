package Pdev;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Client
 *
 */
@Entity

public class Client extends User implements Serializable {

	
	private String Image;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="client")
	private List<ArtWork> listArtWorkC ;

	public Client() {
		super();
	}   
	public String getImage() {
		return this.Image;
	}

	public void setImage(String Image) {
		this.Image = Image;
	}
	public List<ArtWork> getListArtWorkC() {
		return listArtWorkC;
	}
	public void setListArtWorkC(List<ArtWork> listArtWorkC) {
		this.listArtWorkC = listArtWorkC;
	}
   
}

package Pdev;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Adress
 *
 */
@Entity

public class Adress implements Serializable {

	   
	@Id
	private int idAdress;
	private String Street;
	private String PostalCode;
	private String Country;
	private String State;
	private static final long serialVersionUID = 1L;
	@OneToOne (mappedBy="adress")
	private User user;
	@OneToOne (mappedBy="adress")
	private ArtGallery artgallery;
	public Adress() {
		super();
	}   
	public int getIdAdress() {
		return this.idAdress;
	}

	public void setIdAdress(int idAdress) {
		this.idAdress = idAdress;
	}   
	public String getStreet() {
		return this.Street;
	}

	public void setStreet(String Street) {
		this.Street = Street;
	}   
	public String getPostalCode() {
		return this.PostalCode;
	}

	public void setPostalCode(String PostalCode) {
		this.PostalCode = PostalCode;
	}   
	public String getCountry() {
		return this.Country;
	}

	public void setCountry(String Country) {
		this.Country = Country;
	}   
	public String getState() {
		return this.State;
	}

	public void setState(String State) {
		this.State = State;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
   
}

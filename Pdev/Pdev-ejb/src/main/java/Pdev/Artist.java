package Pdev;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

import Pdev.ArtWork;

/**
 * Entity implementation class for Entity: Artist
 *
 */
@Entity

public class Artist extends User implements Serializable {

	
	private Long Biographie;
	private String Image;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="artist")
	private List<ArtWork> listArtWork ;
	@OneToMany(mappedBy="artist2")
	private List<Location> listLocation; 
	

	public Artist() {
		super();
	}   
	public Long getBiographie() {
		return this.Biographie;
	}

	public void setBiographie(Long Biographie) {
		this.Biographie = Biographie;
	}   
	public String getImage() {
		return this.Image;
	}

	public void setImage(String Image) {
		this.Image = Image;
	}
	public List<ArtWork> getListArtWork() {
		return listArtWork;
	}
	public void setListArtWork(List<ArtWork> listArtWork) {
		this.listArtWork = listArtWork;
	}
   
}
